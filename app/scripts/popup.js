/**
 * Created by SelcetStudio on 3/29/16.
 */

(function () {
  var itemWrapper = document.getElementsByClassName('share-it')[0];
  var btnItem = document.getElementsByClassName('share-it-btn')[0];
  var closeBtn = document.getElementsByClassName('close-btn')[0];


  function toggle() {
    if (itemWrapper.classList.contains('active')) {
      itemWrapper.classList.remove('active');
    } else {
      itemWrapper.classList.add('active');
    }
  }

  btnItem.addEventListener('click', toggle);
  closeBtn.addEventListener('click', toggle);
})();